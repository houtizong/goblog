package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["gblog/controllers/admin:AdminController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:AdminController"],
        beego.ControllerComments{
            Method: "Admin",
            Router: "/admin/home",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:AdminController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:AdminController"],
        beego.ControllerComments{
            Method: "Login",
            Router: "/admin/login",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:AdminController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:AdminController"],
        beego.ControllerComments{
            Method: "Info",
            Router: "/admin/login",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:AdminController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:AdminController"],
        beego.ControllerComments{
            Method: "Test",
            Router: "/admin/test",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:CasbinController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:CasbinController"],
        beego.ControllerComments{
            Method: "Roles",
            Router: "/admin/casbin/roles",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:CasbinController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:CasbinController"],
        beego.ControllerComments{
            Method: "Rule_add",
            Router: "/admin/casbin/rule_add",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:CasbinController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:CasbinController"],
        beego.ControllerComments{
            Method: "Rules",
            Router: "/admin/casbin/rules",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:CasbinController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:CasbinController"],
        beego.ControllerComments{
            Method: "Users",
            Router: "/admin/casbin/users",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:TasksController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:TasksController"],
        beego.ControllerComments{
            Method: "Starttask",
            Router: "/admin/starttask",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:TasksController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:TasksController"],
        beego.ControllerComments{
            Method: "Stoptask",
            Router: "/admin/stoptask",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:TasksController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:TasksController"],
        beego.ControllerComments{
            Method: "Task",
            Router: "/admin/task",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gblog/controllers/admin:TasksController"] = append(beego.GlobalControllerRouter["gblog/controllers/admin:TasksController"],
        beego.ControllerComments{
            Method: "Tasks",
            Router: "/admin/tasks",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
