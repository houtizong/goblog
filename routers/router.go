package routers

import (
	"gblog/controllers"
	"gblog/controllers/admin"
	"gblog/controllers/api"

	"gblog/middleware"

	"github.com/astaxie/beego"
)

func init() {
	//授权登录中间件
	middleware.Auth()

	//首页
	beego.Router("/", &controllers.IndexController{})
	beego.Router("/date/:date.html", &controllers.IndexController{})
	//关于我们
	beego.Router("/about", &controllers.IndexController{}, "get:About")
	//IP地址定位
	beego.Router("/iptolocation", &controllers.IndexController{}, "get:IpToLocation;post:IpToLocation")

	//栏目
	beego.Router("/cat/:id", &controllers.IndexController{})

	//测试相关
	beego.Router("/test", &controllers.TestController{}, "get:Test")
	beego.Router("/test/aaa", &controllers.TestController{}, "get:Aaa")
	beego.Router("/test/json2csv", &controllers.TestController{}, "get:Json2csv")
	beego.Router("/test/csv2json", &controllers.TestController{}, "get:Csv2json")

	//个人中心
	beego.Router("/user", &controllers.UserController{}, "get:User")
	//编辑用户信息 头像/密码...
	beego.Router("/edituserface", &controllers.UserController{}, "get:Edituserface;post:Edituserface")
	beego.Router("/edituserpwd", &controllers.UserController{}, "get:Edituserpwd;post:Edituserpwd")

	beego.Router("/reg", &controllers.UserController{}, "get:Reg")
	beego.Router("/register", &controllers.UserController{}, "post:Register")

	beego.Router("/login", &controllers.UserController{}, "get:Login")
	beego.Router("/logout", &controllers.UserController{}, "get:Logout")

	beego.Router("/info", &controllers.UserController{}, "post:Info")
	beego.Router("/infolist", &controllers.UserController{}, "get:InfoList;post:InfoList")

	//发邮件
	beego.Router("/sendemail", &controllers.UserController{}, "get:SendEmail")

	//文章相关
	beego.Router("/art/:id.html", &controllers.ArtController{}, "get:Detail")
	//创建文章
	beego.Router("/addart", &controllers.ArtController{}, "get:Addart;post:Addart")
	//编辑文章
	beego.Router("/editart", &controllers.ArtController{}, "get:Editart;post:Editart")

	//标签相关
	beego.Router("/tags", &controllers.TagsController{}, "get:Tags")
	beego.Router("/tag/:id.html", &controllers.TagsController{}, "get:Tag")

	//聊天
	beego.Router("/chat", &controllers.ServerController{})
	beego.Router("/chat/ws", &controllers.ServerController{}, "get:WS")
	//私聊
	beego.Router("/chatsl", &controllers.ServerController{}, "get:ChatSl")

	//控制器分层之api目录路由
	beego.Router("/api/test", &api.TestController{}, "get:Get")

	//后台管理路由
	//使用注解路由
	beego.Include(&admin.AdminController{})
	beego.Include(&admin.CasbinController{})
	//定时任务
	beego.Include(&admin.TasksController{})

	//静态资源
	beego.SetStaticPath("/image", "static/img/image")
	beego.SetStaticPath("/face", "static/img/face")

}
