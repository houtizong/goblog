#基于golang镜像包
FROM golang 

MAINTAINER Razil "houtizong"

#在go环境下创建一个网站目录
#RUN mkdir -p /go/src/gblog 
#设置工作目录
WORKDIR /go/src 

# 由于所周知的原因，某些包会出现下载超时。这里在docker里也使用go module的代理服务
ENV GO111MODULE=on
ENV GOPROXY="https://goproxy.io"

RUN apt update
#下载vim编辑器
RUN apt install -y vim 
#下载网络工具包
#RUN apt install -y net-tools 

#安装beego
RUN go get github.com/astaxie/beego 
#安装bee
RUN go get github.com/beego/bee 

#在工作目录下创建一个goblog项目
#RUN bee new goblog 



#测试go环境
#WORKDIR $GOPATH/src/gblog
#ADD . $GOPATH/src/gblog
#RUN go build main.go
#EXPOSE 8080
#ENTRYPOINT ["./main"]