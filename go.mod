module gblog

go 1.15

require github.com/astaxie/beego v1.12.3

require (
	github.com/beego/beego/v2 v2.0.1
	github.com/casbin/beego-orm-adapter/v3 v3.0.2
	github.com/casbin/casbin/v2 v2.44.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/oschwald/geoip2-golang v1.7.0
	github.com/prometheus/client_golang v1.12.1 // indirect
	github.com/shiena/ansicolor v0.0.0-20200904210342-c7312218db18 // indirect
	github.com/smartystreets/goconvey v1.6.4
	golang.org/x/crypto v0.0.0-20220321153916-2c7772ba3064 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
