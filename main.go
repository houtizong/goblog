package main

import (
	"gblog/components"
	"gblog/controllers"
	_ "gblog/routers"
	"gblog/utils"

	"context"
	"fmt"
	"time"

	"github.com/astaxie/beego"
)

func init() {
	components.InitLogger() //调用logger初始化
	utils.Init()            //注册函数
}

func main() {
	//404页面
	beego.ErrorController(&controllers.ErrorController{})

	//指定时长超时结束 3秒后结束
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*3)
	defer cancel() // 防止任务比超时时间短导致资源未释放
	go clock(ctx)

	beego.Run()
}

func clock(ctx context.Context) {
	ticker := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			fmt.Println("1 seconds")
		}
	}
}
