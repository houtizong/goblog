package models

import (
	"github.com/astaxie/beego/orm"
)

//task表显示的字段
type Task struct {
	Id       int `orm:"pk"`
	Taskname string
	Spec     string
	Desc     string
	Addtime  string
	Status   int
	//Children    []Comment `orm:"-"` //加个xx省略映射关系
}

func EditTask(task *Task) (int64, error) {
	o := orm.NewOrm()
	num, err := o.Update(task)
	return num, err
}

func AddTask(task *Task) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(task)
	return id, err
}
