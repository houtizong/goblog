package models

import (
	"github.com/astaxie/beego/orm"
)

//art表需显示的字段
type Art struct {
	Art_id   int `orm:"column(art_id);pk"` // 设置主键
	Title    string
	Content  string
	Cat_id   int
	Author   string
	User_id  int
	Tags     string
	Pubtime  string
	Is_state int
	Lastup   int
	Comm     int
	View     int
}

//方法 编辑
func EditArt(art *Art) (int64, error) {
	o := orm.NewOrm()
	num, err := o.Update(art)
	return num, err
}

//方法 添加
func AddArt(art *Art) (int64, error) {
	o := orm.NewOrm()
	//art := new(Art)
	//art.Profile = profile
	//art.Name = "slene"
	id, err := o.Insert(art)
	return id, err
}
