package models

type Role struct {
	Id   int    `orm:"column(id);auto;pk" description:"角色序号"`
	Name string `orm:"unique"  description:"角色名"`
	Desc string
}
