//公共函数
package models

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"

	"math/rand"
	"time"

	"io"
	"os"

	"github.com/astaxie/beego"
	config "github.com/astaxie/beego/config"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gomail.v2"
)

//统一返回json string
func ReturnJson(code string, msg string, data interface{}) string {
	res := map[string]string{"Code": code, "Msg": msg}
	j, _ := json.Marshal(res)
	return string(j)
}

//分页方法，根据传递过来的页数，每页数，总数，返回分页的内容 7个页数 前 1，2，3，4，5 后 的格式返回,小于5页返回具体页数
func Paginator(page, prepage int, nums int) map[string]interface{} {

	var firstpage int //前一页地址
	var lastpage int  //后一页地址
	//根据nums总数，和prepage每页数量 生成分页总数
	totalpages := int(math.Ceil(float64(nums) / float64(prepage))) //page总数
	if page > totalpages {
		page = totalpages
	}
	if page <= 0 {
		page = 1
	}
	var pages []int
	switch {
	case page >= totalpages-5 && totalpages > 5: //最后5页
		start := totalpages - 5 + 1
		firstpage = page - 1
		lastpage = int(math.Min(float64(totalpages), float64(page+1)))
		pages = make([]int, 5)
		for i, _ := range pages {
			pages[i] = start + i
		}
	case page >= 3 && totalpages > 5:
		start := page - 3 + 1
		pages = make([]int, 5)
		firstpage = page - 3
		for i, _ := range pages {
			pages[i] = start + i
		}
		firstpage = page - 1
		lastpage = page + 1
	default:
		pages = make([]int, int(math.Min(5, float64(totalpages))))
		for i, _ := range pages {
			pages[i] = i + 1
		}
		firstpage = int(math.Max(float64(1), float64(page-1)))
		lastpage = page + 1
	}
	paginatorMap := make(map[string]interface{})
	paginatorMap["pages"] = pages
	paginatorMap["totalpages"] = totalpages
	paginatorMap["firstpage"] = firstpage
	paginatorMap["lastpage"] = lastpage
	paginatorMap["currpage"] = page
	return paginatorMap
}

//调用 a := models.SendEmail(a,b,c)
func SendEmail(toemail string) bool {

	//读取配置文件信息
	iniconf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		beego.Error(err)
	}

	host := iniconf.String("MAIL_HOST")
	post := iniconf.String("MAIL_PORT")
	from := iniconf.String("MAIL_USERNAME")
	tk := iniconf.String("MAIL_PASSWORD")
	fromname := iniconf.String("MAIL_FROM_NAME")

	m := gomail.NewMessage()
	//发送人
	m.SetHeader("From", from)
	//接收人
	m.SetHeader("To", toemail)
	//抄送人
	//m.SetAddressHeader("Cc", "houtizong@163.com", "抄送")
	//主题
	m.SetHeader(fromname, "beego发邮箱ces")
	//内容
	m.SetBody("text/html", "<h1>beego发邮箱111</h1>")
	//附件
	//m.Attach("./xx.png")

	//需要转一下类型
	intpost, err := strconv.Atoi(post)

	//拿到token，并进行连接,第4个参数是填授权码
	d := gomail.NewDialer(host, intpost, from, tk)

	// 发送邮件
	if err := d.DialAndSend(m); err != nil {
		fmt.Printf("send mail err %v:", err)
		panic(err)
		return false
	}

	fmt.Printf("send mail success")
	return true
}

//md5加密string 调用a := models.Md5("123456")
func Md5(str string) string {
	data := []byte(str)
	return fmt.Sprintf("%x", md5.Sum(data))
}

//正则验证邮箱格式 调用a := models.VerifyEmail("514224527@qq.com")
func VerifyEmail(email string) bool {
	//匹配电子邮箱
	pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*`
	isemail := regexp.MustCompile(pattern)
	return isemail.MatchString(email)
}

//调用 a := models.Aaaa(art.Pubtime)
func Aaaa(str string) (re string) {
	re = "测试"
	return re
}

//写入文件信息  调用a := models.WriteFile("testaaa.txt", "./logs/", "xxxxxxxxxxxxx")
func WriteFile(filename string, path string, str string) bool {
	//绝对路径
	pf := path + filename

	file, err := os.Create(pf)
	if err != nil {
		fmt.Println(err)
	}

	//测试打印
	fmt.Println(" Write to file : " + pf)

	n, err := io.WriteString(file, str)
	if err != nil {
		fmt.Println(n, err)
	}

	file.Close()

	return true
}

//在指定的列数处换行文本
// s string 字符串文本数据
// limit int 限制单词个数
func WordWrap(s string, limit int) string {

	if strings.TrimSpace(s) == "" {
		return s
	}

	//将字符串转换为切片
	strSlice := strings.Fields(s)
	var result string = ""

	for len(strSlice) >= 1 {
		// 将切片/数组转换回字符串
		// 但在指定的限制处插入 \r\n
		result = result + strings.Join(strSlice[:limit], " ") + "\r\n"

		//丢弃复制到结果的元素
		strSlice = strSlice[limit:]

		// 改变限制
		// 满足最后几个单词
		if len(strSlice) < limit {
			limit = len(strSlice)
		}
	}

	return result
}

//对列表数组进行随机/洗牌
func ShuffleList(start, end int) []int {
	if end < start {
		start, end = end, start
	}

	length := end - start
	rand.Seed(time.Now().UTC().UnixNano())

	list := rand.Perm(length)
	for index, _ := range list {
		list[index] += start
	}
	return list
}

//根据n生成随机指定位数数字字符串
func RandomIntString(n int) string {
	timeUnix := time.Now().Unix()
	timestamp := time.Unix(timeUnix, 0).Format("20060102150405")
	var letters = []rune(timestamp)
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

//根据min max生成它们之间的随机数
func RandomInt(min int, max int) int {
	//rand.Seed(time.Now().UnixNano())
	number := rand.Intn(max-min) + min
	return number
}
