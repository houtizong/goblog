package models

type Admin struct {
	// 后台用户模型
	Id       int    `orm:"auto;pk" description:"用户序号" json:"id"`
	Username string `orm:"unique" description:"用户名" json:"username"`
	Password string `description:"用户密码" json:"password"`
	Role_id  int
	Status   int
}
