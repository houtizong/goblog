package models

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

//由于model这个名字叫 Users 那么操作的表其实 users
type Users struct {
	Id                 int64
	Name               string
	Email              string
	Password           string
	Created_at         string
	Profile_photo_path string
}

//方法 添加
func AddUser(users *Users) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(users)
	return id, err
}

//方法 编辑
func EditUser(users *Users) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Update(users)
	return id, err
}
