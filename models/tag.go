package models

import (
	"github.com/astaxie/beego/orm"
)

//tag表需显示的字段
type Tag struct {
	Tag_id  int `orm:"column(tag_id);pk"` // 设置主键
	Art_id  int
	Tagname string
	Addtime string
}

//方法 编辑
func EditTag(tag *Tag) (int64, error) {
	o := orm.NewOrm()
	num, err := o.Update(tag)
	return num, err
}

//方法 添加
func AddTag(tag *Tag) (int64, error) {
	o := orm.NewOrm()
	id, err := o.Insert(tag)
	return id, err
}

//批量插入  ps:InsertMulti最多100条
func AddsTag(tags []Tag) (int64, error) {
	o := orm.NewOrm()
	nums, err := o.InsertMulti(len(tags), tags)
	return nums, err
}

//方法 删除 ps:参数Id可以省略，其他需传进来,我这里不做优化
func Deltag(tag *Tag, val string) (int64, error) {
	o := orm.NewOrm()
	num, err := o.Delete(tag, val)
	return num, err
}
