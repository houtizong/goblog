package models

//comment表显示的字段
type Comment struct {
	Comment_id  int `orm:"column(comment_id);pk"`
	Fcomment_id int
	Art_id      int
	User_id     int
	Username    string
	Face        string
	Comment     string
	Pubtime     string
	//Children    []Comment `orm:"-"` //加个tag省略映射关系
}
