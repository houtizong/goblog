package models

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

func init() {

	orm.RegisterModel(new(Users), new(Art), new(Tag), new(Comment))
	orm.RegisterModel(new(Task))
	orm.RegisterModel(new(Admin), new(Role), new(CasbinRule))

	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", "root:Cbb_0721!@#$@tcp(192.168.1.140:3306)/beego?charset=utf8", 30)
	//orm.RunSyncdb("default", false, false)
	orm.Debug = true // 是否开启调试模式 调试模式下会打印出sql语句
}
