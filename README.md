﻿# goblog

#### 介绍
go语言+beego框架体验，编写博客网站

测试地址:http://go.zongscan.com

本人博客：https://www.zongscan.com/

#### 软件架构
docker + beego + mysql

#### 功能
1.后台及登录
2.后台实现简单权限 casbin RBAC


#### 安装教程 
#克隆项目

cd /home/godocker/src/

git clone https://gitee.com/houtizong/goblog.git

#返回/home/godocker/目录

cd -

#构建镜像

docker build -t gblog .

#启动容器项目 （命令参数意思请自行上网查阅）

docker run --init -p 8080:8080 -itd --rm --name gblogbeego -v "/home/godocker/src":/go/src/ -w /go/src/gblog gblog bee run

#查看镜像

docker images

#查看容器

docker ps -a

#进入容器

docker exec -it 容器ID /bin/sh

#重启容器 （修改代码/拉取新代码 需执行该命令）

docker restart 容器ID


##具体操作请访问：
https://www.zongscan.com/demo333/89352.html


#### 使用说明

##如果不用docker构建，直接运行的话

##可改成守护进程

cd /home/goblog/src/gblog/

nohup go run main.go &


##linux命令

show

ps -ef

kill pid

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
