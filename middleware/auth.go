package middleware

import (
	"fmt"
	"gblog/models"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"

	beegoormadapter "github.com/casbin/beego-orm-adapter/v3"
	"github.com/casbin/casbin/v2"
	_ "github.com/go-sql-driver/mysql"
)

// 中间件
func Auth() {
	//admin请求前拦截器
	// var filterFunc = func(ctx *context.Context) {
	// 	adminid := ctx.Input.Session("adminid")
	// 	fmt.Println(adminid, ctx.Request.RequestURI, ctx.Request)
	// 	if adminid == nil && ctx.Request.RequestURI != "/admin/login" {
	// 		ctx.Redirect(302, "/admin/login")
	// 	}
	// }
	// beego.InsertFilter("/admin/*", beego.BeforeRouter, filterFunc)

	//引人casbin admin拦截器
	e := Casbin()
	beego.InsertFilter("/admin/*", beego.BeforeRouter, Authorizer(e))
}

//Casbin使用的都是调用这个变量
var Enforcer *casbin.Enforcer

func Casbin() *casbin.Enforcer {
	a, _ := beegoormadapter.NewAdapter("default", "mysql", "root:Cbb_0721!@#$@tcp(192.168.1.140:3306)/beego")
	e, _ := casbin.NewEnforcer("conf/rbac_model.conf", a)
	// Enforcer初始化 - 传入对象
	Enforcer = e
	return Enforcer
}

func Authorizer(e *casbin.Enforcer) beego.FilterFunc {
	return func(ctx *context.Context) {
		// 获取用户角色
		admins, ok := ctx.Input.Session("admins").(models.Admin)
		//检查是否登录
		if !ok && ctx.Request.RequestURI != "/admin/login" {
			ctx.Redirect(302, "/admin/login")
		}
		user := admins.Username
		role := strconv.Itoa(admins.Role_id)

		// 获取访问路径
		method := strings.ToLower(ctx.Request.Method)
		// 获取访问方式
		path := strings.ToLower(ctx.Request.URL.Path)
		fmt.Println(user, role, path, method, "权限参数打印")

		// sub := "test"      // 想要访问资源的用户/角色。
		// obj := "get"       // 将被访问的资源/用户/角色。
		// act := "/ad/test1" // 用户对资源执行的操作。

		//ps: 继承关系-前面继承后面 -- 角色继承用户/角色继承角色
		roles, err := e.GetRolesForUser(user)
		fmt.Println(roles, err, "用户是什么角色")

		if ok, _ := e.Enforce(role, path, method); ok {
			fmt.Println("恭喜您,权限验证通过")
		} else {
			if path != "/admin/login" {
				ctx.Output.JSON(map[string]string{"msg": "用户权限不足"}, true, false)
			}
			fmt.Println("很遗憾,权限验证没有通过")
		}

	}
}
