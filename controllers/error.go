package controllers

/**
  该控制器处理页面错误请求
*/
type ErrorController struct {
	BaseController
}

func (c *ErrorController) Error404() {
	c.Data["content"] = "很抱歉您访问的地址或者方法不存在"
	c.TplName = "error/404.html"
}
