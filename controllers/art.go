package controllers

import (
	"fmt"
	"gblog/models"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type ArtController struct {
	BaseController
}

//文章详情页
func (c *ArtController) Detail() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)

	o := orm.NewOrm()

	//文章
	art := models.Art{}
	o.Raw("SELECT * FROM art WHERE art_id = ?", id).QueryRow(&art)
	c.Data["art"] = art

	//更新view 自增
	art.Art_id = id
	art.View = art.View + 1
	models.EditArt(&art)

	c.Data["title"] = art.Title + "-侯体宗的博客"
	c.Data["keywords"] = art.Title
	c.Data["description"] = art.Title

	//a := models.Aaaa(art.Pubtime)
	//c.Data["json"] = a
	//c.ServeJSON()
	//c.StopRun()

	//评论
	var commdates []orm.Params
	commdatessql := `select * from comment where art_id = ?`
	o.Raw(commdatessql, art.Art_id).Values(&commdates)
	c.Data["commdates"] = commdates

	//无极限评论测试数据
	// arrs := []comm{
	// 	{Comment_id: 1, Fcomment_id: 0, Art_id: 11, user_id: 11, Username: "11", face: "11", comment: "11", pubtime: "11"},
	// 	{Comment_id: 2, Fcomment_id: 0, Art_id: 22, user_id: 22, Username: "22", face: "22", comment: "22", pubtime: "22"},
	// 	{Comment_id: 3, Fcomment_id: 2, Art_id: 33, user_id: 33, Username: "我是2的子级", face: "33", comment: "33", pubtime: "33"},
	// }

	//初始化orm数据集 - 统一类型 - 结构体
	s := []comm{}

	for _, v := range commdates {
		comment_id, _ := strconv.Atoi(v["comment_id"].(string))
		fcomment_id, _ := strconv.Atoi(v["fcomment_id"].(string))
		art_id, _ := strconv.Atoi(v["art_id"].(string))
		zan, _ := strconv.Atoi(v["zan"].(string))
		node := comm{
			Comment_id:  comment_id,
			Fcomment_id: fcomment_id,
			Art_id:      art_id,
			Username:    v["username"].(string),
			Comment:     v["comment"].(string),
			Pubtime:     v["pubtime"].(string),
			Zan:         zan,
		}
		s = append(s, node)
	}
	fmt.Println(s)

	//无极限处理评论数据
	list := SonsTree(s, 0)
	fmt.Println(list)
	c.Data["list"] = list

	//c.Data["json"] = list
	//c.ServeJSON()
	//c.StopRun()

	// comment := []models.Comment{}
	// o.Raw("SELECT * FROM comment WHERE art_id = ?", art.Art_id).QueryRows(&comment)

	//切片 定义关联数组 k=>v
	// m := make(map[int]map[string]interface{})
	// sub := make(map[int]map[string]interface{})

	// for k, v := range commdates {
	// 	m[k] = make(map[string]interface{})
	// 	//元素不能为null,空
	// 	m[k]["comment_id"] = v["comment_id"].(string)
	// 	m[k]["fcomment_id"] = v["fcomment_id"].(string)
	// 	m[k]["art_id"] = v["art_id"].(string)
	// 	m[k]["user_id"] = v["user_id"].(string)
	// 	m[k]["username"] = v["username"].(string)
	// 	//m[k]["face"] = v["face"].(string)
	// 	m[k]["comment"] = v["comment"].(string)
	// 	m[k]["pubtime"] = v["pubtime"].(string)
	// 	//子评论
	// 	//sub := make(map[int]map[string]string)
	// 	for _, vv := range commdates {
	// 		if v["comment_id"] == vv["fcomment_id"] {
	// 			cid, _ := strconv.Atoi(vv["comment_id"].(string))
	// 			//m[k]["sub"] = map[int]map[string]string{cid: {"comment_id": vv["comment_id"].(string), "username": vv["username"].(string)}}
	// 			//fmt.Println(cid, v["username"], v["comment_id"])

	// 			sub[cid] = make(map[string]interface{})
	// 			sub[cid]["comment_id"] = vv["comment_id"].(string)
	// 			sub[cid]["username"] = vv["username"].(string)
	// 			sub[cid]["fcomment_id"] = vv["fcomment_id"].(string)
	// 			m[k]["sub"] = sub
	// 		}
	// 	}
	// }
	// fmt.Println(m)

	//文章归档 格式：2017年01月
	var artdates []orm.Params
	artdatessql := `select pubtime,FROM_UNIXTIME( pubtime,'%Y-%m') as time ,count(*) as num FROM art where cat_id = ? and is_state=0 and is_del=1 group by time`
	o.Raw(artdatessql, art.Cat_id).Values(&artdates)
	c.Data["artdates"] = artdates

	//登录用户名
	//username := c.GetSession("username")
	//c.Data["username"] = username

	c.TplName = "detail.html"
}

//编辑数据
func (c *ArtController) Editart() {
	//注：
	//获取path参数，如 /editart/{id} 用c.Ctx.Input.Param(":id")
	//获取query参数，如 /?id=1 用c.Ctx.Input.Query("id")
	idStr := c.Ctx.Input.Query("id")
	id, _ := strconv.Atoi(idStr)

	o := orm.NewOrm()

	//文章
	art := models.Art{Art_id: id}
	err := o.Read(&art)
	if err != nil {
		c.TplName = "login.html"
		return
	}
	c.Data["art"] = art

	if c.Ctx.Request.Method == "POST" {

		//验form表单数据... to do
		title := c.GetString("title")
		content := c.GetString("content")
		tags := c.GetString("tags")
		cat_id, _ := strconv.Atoi(c.GetString("cat_id"))

		//编辑入库
		art.Title = title
		art.Content = content
		art.Tags = tags
		art.Cat_id = cat_id
		num, err := models.EditArt(&art)
		if err != nil {
			c.ErrorJson(500, "编辑失败", nil)
		}

		//删除旧标签
		tag := models.Tag{}
		tag.Art_id = id
		count, _ := models.Deltag(&tag, "Art_id")
		fmt.Println("edit", num, count)

		//获取时间戳
		s := time.Now().Unix()
		timestamp := strconv.FormatInt(int64(s), 10)

		//添加新标签
		ss := strings.Split(tags, ",")
		for i := 0; i < len(ss); i++ {
			tag := models.Tag{Art_id: int(id), Tagname: ss[i], Addtime: timestamp}
			models.AddTag(&tag)
		}

		c.SuccessJson("编辑成功")
	}

	c.TplName = "addart.html"
}

//添加数据
func (c *ArtController) Addart() {
	//1.拿到数据
	title := c.GetString("title")
	content := c.GetString("content")
	tags := c.GetString("tags")
	cat_id, _ := strconv.Atoi(c.GetString("cat_id"))
	//session user信息
	username := c.GetSession("username").(string)
	//接口类型interface int64转int
	userid := int(c.GetSession("userid").(int64))

	if c.Ctx.Request.Method == "POST" {
		//2.对数据进行校验
		if title == "" && content == "" {
			c.ErrorJson(500, "标题/内容不能为空", nil)
		}
		//获取时间戳
		s := time.Now().Unix()
		timestamp := strconv.FormatInt(int64(s), 10)

		art := models.Art{Title: title, Content: content, Cat_id: cat_id, Author: username, User_id: userid, Tags: tags, Pubtime: timestamp, Is_state: 0}
		id, err := models.AddArt(&art)
		if err != nil {
			c.ErrorJson(500, "发布失败", nil)
		}
		//插入标签
		ss := strings.Split(tags, ",")
		for i := 0; i < len(ss); i++ {
			tag := models.Tag{Art_id: int(id), Tagname: ss[i], Addtime: timestamp}
			models.AddTag(&tag)
		}
		// //批量插入标签
		// tag := models.Tag{}
		// s := make([]models.Tag, 0)

		// for _, v := range ss {
		// 	tag.Art_id = int(id)
		// 	tag.Tagname = v
		// 	tag.Addtime = timestamp

		// 	s = append(s, tag)
		// }
		// //批量插入模型
		// models.AddsTag(s)

		//改成事务方式

		fmt.Println(art, id, err)
		c.SuccessJson("发布成功")
	}
	fmt.Println("6666", title, cat_id, content, tags)

	c.TplName = "addart.html"
}

type comm struct {
	Comment_id  int
	Fcomment_id int
	Art_id      int
	User_id     int
	Username    string
	Face        string
	Comment     string
	Pubtime     string
	Zan         int
	Children    []comm
}
type Son struct {
	Comment_id  int
	Fcomment_id int
	Art_id      int
	User_id     int
	Username    string
	Face        string
	Comment     string
	Pubtime     string
	Zan         int
	Children    []Son
}

//无极限获取子孙树
func SonsTree(comm []comm, pid int) []Son {
	//fmt.Println(comm)
	s := []Son{}

	for _, v := range comm {
		if v.Fcomment_id == pid {
			child := SonsTree(comm, v.Comment_id)
			node := Son{
				Comment_id:  v.Comment_id,
				Fcomment_id: v.Fcomment_id,
				Art_id:      v.Art_id,
				Username:    v.Username,
				Comment:     v.Comment,
				Pubtime:     v.Pubtime,
				Zan:         v.Zan,
			}
			node.Children = child
			s = append(s, node)
		}
	}

	return s
}
