package controllers

import (
	"fmt"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

type BaseController struct {
	beego.Controller
}

//init只会在初次引用时执行一次，而Prepare会在每次调用中都执行一次
func (c *BaseController) Prepare() {

	//根据url做个简单的权限判断控制
	//获取url 拆分url
	urlarr := strings.Split(c.Ctx.Request.RequestURI, "?")
	url := urlarr[0]

	//定义数组 存储需要登录的url
	urls := [7]string{
		"/infolist",
		"/user",
		"/edituserface",
		"/edituserpwd",
		"/addart",
		"/editart",
	}
	//判断是否在数组
	exists := in_Array(len(urls), func(i int) bool {
		return urls[i] == url
	})
	//获取session
	username := c.GetSession("username")
	userid := c.GetSession("userid")
	useremail := c.GetSession("useremail")
	//进行跳转
	if exists == true && username == nil {
		c.Ctx.Redirect(302, "/login") //若Session中无用户ID则302重定向至登陆页面
	}
	//session赋值
	c.Data["username"] = username
	c.Data["userid"] = userid
	c.Data["useremail"] = useremail

	fmt.Println(urls)
	fmt.Println(url)
	fmt.Println(exists)

	//orm 全局数据 start
	o := orm.NewOrm()

	//分类列表
	var cats []orm.Params
	catssql := `select cat.cat_id,cat.catname,count(art.cat_id) as num from cat inner join art on art.cat_id = cat.cat_id group by art.cat_id`
	o.Raw(catssql).Values(&cats)
	c.Data["cats"] = cats

	//标签云
	var tags []orm.Params
	tagssql := `select tag_id,tagname FROM tag group by tagname order by tag_id desc limit 50`
	o.Raw(tagssql).Values(&tags)
	c.Data["tags"] = tags

	//orm 全局数据 end
}

// 判断是否在数组
func in_Array(n int, f func(int) bool) bool {
	for i := 0; i < n; i++ {
		if f(i) {
			return true
		}
	}
	return false
}

//统一返回json
type ReturnMsg struct {
	Code int
	Msg  string
	Data interface{}
}

func (c *BaseController) SuccessJson(data interface{}) {
	res := ReturnMsg{
		200, "success", data,
	}
	c.Data["json"] = res
	c.ServeJSON() //对json进行序列化输出
	c.StopRun()
}

func (c *BaseController) ErrorJson(code int, msg string, data interface{}) {
	res := ReturnMsg{
		code, msg, data,
	}
	c.Data["json"] = res
	c.ServeJSON() //对json进行序列化输出
	c.StopRun()
}
