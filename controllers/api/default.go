package api

import (
	"fmt"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/httplib"
)

type TestController struct {
	beego.Controller
}

func (c *TestController) Get() {
	//c.Data["Website"] = "beego.me"
	//c.Data["Email"] = "astaxie@gmail.com"
	//c.TplName = "index.tpl"

	//get
	//req := httplib.Get("http://127.0.0.1:8080/admin/home")
	//post
	req := httplib.Post("http://127.0.0.1:8080/admin/login")
	//传参
	req.Param("name", "admin")
	req.Param("password", "111111")

	str, err := req.String()

	//解析为JSON结构
	var json interface{}
	req.ToJSON(&json)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(str, json)

	c.Ctx.WriteString(str)
}
