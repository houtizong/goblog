package controllers

import (
	"fmt"
	"gblog/models"
	"net"
	"strconv"

	"github.com/oschwald/geoip2-golang"

	"github.com/astaxie/beego/orm"
)

type IndexController struct {
	BaseController
}

func (c *IndexController) Get() {

	c.Data["title"] = "Go编程_beego_常用PHP框架_前端后端分享交流-侯体宗的博客"
	c.Data["keywords"] = "Go编程_beego_常用PHP框架_前端后端分享交流"
	c.Data["description"] = "个人博客,侯体宗的博客,Go编程,beego框架,侯体宗的博客常用框架及前端后端分享交流"

	o := orm.NewOrm()

	//当前页码
	page, err := c.GetInt("page")
	if err != nil {
		page = 1
		println(err)
	}

	date := c.Ctx.Input.Param(":date")
	cat_id := c.Ctx.Input.Param(":id")

	if date != "" {
		println(date)

		//文章总条数
		var count []orm.Params
		numssql := `select count(*) as count from art where FROM_UNIXTIME(pubtime,'%Y-%m')=? and is_state=0 and is_del=1 order by art_id desc`
		o.Raw(numssql, date).Values(&count)
		nums := count[0]["count"]
		// 这里先显示转换，.(string) 把interface转换成string类型，再利用strconv.Atoi把string 转换成int
		cnt, _ := strconv.Atoi(nums.(string))
		//当前页 每页条数 总条数
		var pageSize int
		pageSize = 30
		pages := models.Paginator(page, pageSize, cnt)
		c.Data["pages"] = pages

		//文章列表 分页数据
		var arts []orm.Params
		artssql := `select art_id,title,content,pubtime,view,cat_id from art where FROM_UNIXTIME(pubtime,'%Y-%m')=? and is_state=0 and is_del=1 order by art_id desc limit ?,?`
		o.Raw(artssql, date, (page-1)*pageSize, pageSize).Values(&arts)
		c.Data["arts"] = arts

	} else if cat_id != "" {
		var count []orm.Params
		numssql := `select count(*) as count from art where cat_id=? and is_state=0 and is_del=1 order by art_id desc`
		o.Raw(numssql, cat_id).Values(&count)
		nums := count[0]["count"]

		cnt, _ := strconv.Atoi(nums.(string))
		//当前页 每页条数 总条数
		var pageSize int
		pageSize = 30
		pages := models.Paginator(page, pageSize, cnt)
		c.Data["pages"] = pages

		//文章列表 分页数据
		var arts []orm.Params
		artssql := `select art_id,title,content,pubtime,view,cat_id from art where cat_id=? and is_state=0 and is_del=1 order by art_id desc limit ?,?`
		o.Raw(artssql, cat_id, (page-1)*pageSize, pageSize).Values(&arts)
		c.Data["arts"] = arts

	} else {

		//文章总条数
		var count []orm.Params
		numssql := `select count(*) as count from art where is_state=0 and is_del=1 order by art_id desc`
		o.Raw(numssql).Values(&count)
		nums := count[0]["count"]
		// 这里先显示转换，.(string) 把interface转换成string类型，再利用strconv.Atoi把string 转换成int
		cnt, _ := strconv.Atoi(nums.(string))
		//当前页 每页条数 总条数
		var pageSize int
		pageSize = 30
		pages := models.Paginator(page, pageSize, cnt)
		c.Data["pages"] = pages

		//文章列表 分页数据
		var arts []orm.Params
		artssql := `select art_id,title,content,pubtime,view,cat_id from art where is_state=0 and is_del=1 order by art_id desc limit ?,?`
		o.Raw(artssql, (page-1)*pageSize, pageSize).Values(&arts)
		c.Data["arts"] = arts

	}

	//文章归档 格式：2017年01月
	var artdates []orm.Params
	artdatessql := `select pubtime,FROM_UNIXTIME( pubtime,'%Y-%m') as time ,count(*) as num FROM art where  is_state=0 and is_del=1 group by time`
	o.Raw(artdatessql).Values(&artdates)
	c.Data["artdates"] = artdates

	//随机数
	c.Data["userrandom"] = models.RandomIntString(3)

	//登录用户名 后续改基类
	//username := c.GetSession("username")
	//c.Data["username"] = username
	//fmt.Printf("%v", username)
	//c.Data["json"] = RandomInt(3)
	//c.ServeJSON()

	c.TplName = "index.html"
}

func (c *IndexController) About() {

	c.Data["title"] = "关于我们-侯体宗的博客"
	c.Data["keywords"] = "关于我们"
	c.Data["description"] = "关于我们"

	c.TplName = "about.html"
}

//ip定位
func (c *IndexController) IpToLocation() {
	c.Data["title"] = " GeoLite2 IP地址定位"
	c.Data["keywords"] = "GeoLite2,IP地址定位"
	c.Data["description"] = "GeoLite2 IP地址定位"

	db, err := geoip2.Open("static/GeoLite2-City.mmdb")

	if err != nil {
		fmt.Println(err)
	}

	defer db.Close()

	if c.Ctx.Request.Method == "POST" {
		ipAddress := c.GetString("ipaddress")

		if ipAddress != "" {
			ip := net.ParseIP(ipAddress)
			record, err := db.City(ip)
			if err != nil {
				fmt.Println(err)
			}

			c.SuccessJson(record)
		}
	}

	c.TplName = "iptolocation.html"
}
