package controllers

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"regexp"

	//"reflect"
	"strconv"

	//"time"

	"gblog/models"
	"os"
)

// TestController  is a test control
type TestController struct {
	//beego.Controller
	BaseController
}

type Persons struct {
	Name string
	Blog string
}

func (c *TestController) Test() {
	//c.Ctx.WriteString(`我是test控制器中的test方法`)

	//var a string = "aaa" //下面使用a 就无需再一次声明及:=

	//json转map
	// jsonstr := `{"name":"houtizong","blog":"www.zongscan.com"}`
	// zmap := make(map[string]interface{})
	// err := json.Unmarshal([]byte(jsonstr), &zmap)
	// if err != nil {
	// 	fmt.Println("反序列话失败")
	// } else {
	// 	fmt.Println(zmap)
	// }

	//json转切片
	// jsonstr := `[{"name":"houtizong","blog":"www.zongscan.com"}]`
	// zslice := make([]map[string]interface{}, 0)
	// err := json.Unmarshal([]byte(jsonstr), &zslice)
	// if err != nil {
	// 	fmt.Println("反序列化失败")
	// } else {
	// 	fmt.Println(zslice)
	// }

	//json转结构体
	jsonstr := `{"name":"houtizong","blog":"www.zongscan.com"}`
	k := Persons{}
	err := json.Unmarshal([]byte(jsonstr), &k)
	if err != nil {
		fmt.Println("反序列化失败")
	} else {
		fmt.Println(k)
	}

}

type Employee struct {
	Name string
	Age  int
	Job  string
}

func (c *TestController) Json2csv() {
	c.Ctx.WriteString(`json2csv`)

	//从logs/data.json文件中读取数据
	jsondatafromfile, err := ioutil.ReadFile("logs/data.json")

	if err != nil {
		fmt.Println(err)
	}

	//解析JSON数据
	var jsondata []Employee
	err = json.Unmarshal([]byte(jsondatafromfile), &jsondata)
	fmt.Println(jsondata)
	if err != nil {
		fmt.Println(err)
	}

	//创建空文件logs/data.csv
	csvdatafile, err := os.Create("logs/data.csv")
	if err != nil {
		fmt.Println(err)
	}
	defer csvdatafile.Close()

	//写入data.csv文件
	writer := csv.NewWriter(csvdatafile)

	for _, worker := range jsondata {
		var record []string
		record = append(record, worker.Name)
		record = append(record, strconv.Itoa(worker.Age))
		record = append(record, worker.Job)
		writer.Write(record)
	}

	//如上的操作还是在缓冲区中写数据，数据还在内存中
	writer.Flush() //将内存中的数据刷回到硬盘保存

}

func (c *TestController) Csv2json() {
	c.Ctx.WriteString(`csv2json`)

	//从CSV文件中读取数据
	csvFile, err := os.Open("logs/data.csv")
	if err != nil {
		fmt.Println(err)
	}
	defer csvFile.Close()

	reader := csv.NewReader(csvFile)
	reader.FieldsPerRecord = -1
	csvData, err := reader.ReadAll()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	var oneRecord Employee
	var allRecords []Employee

	for _, each := range csvData {
		oneRecord.Name = each[0]
		oneRecord.Age, _ = strconv.Atoi(each[1]) //需要将整数转换为字符串
		oneRecord.Job = each[2]
		allRecords = append(allRecords, oneRecord)
	}
	//转换为 JSON
	jsondata, err := json.Marshal(allRecords)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// 注意：您也可以将 JSON 数据流式传输到 http 服务，而不是保存到文件
	fmt.Println(string(jsondata))

	//现在写入JSON文件
	jsonFile, err := os.Create("logs/data.json")

	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	jsonFile.Write(jsondata)
	jsonFile.Close()
}

//计算坐标距离 经纬度
type Coordinates struct {
	Latitude  float64
	Longitude float64
}

//地球的平均半径以千米为单位
const radius = 6371

//度数转弧度公式
func degrees2radians(degrees float64) float64 {
	return degrees * math.Pi / 180
}

//Haversine公式(半正矢公式):计算两点之间的直线距离
func (origin Coordinates) Distance(destination Coordinates) float64 {
	degreesLat := degrees2radians(destination.Latitude - origin.Latitude)
	degreesLong := degrees2radians(destination.Longitude - origin.Longitude)
	a := (math.Sin(degreesLat/2)*math.Sin(degreesLat/2) +
		math.Cos(degrees2radians(origin.Latitude))*
			math.Cos(degrees2radians(destination.Latitude))*math.Sin(degreesLong/2)*
			math.Sin(degreesLong/2))
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	d := radius * c

	return d
}

func (c *TestController) Aaa() {

	c.Ctx.WriteString(`我是test控制器中的aaa方法`)

	s := models.ReturnJson("200", "我是中国人", nil)
	fmt.Println("sss", s)

	str := "080218 GG"
	//注意：标记了s , 表达式\n 换成 .
	ret := regexp.MustCompile(`(?s:\d{6}.[SS|GG|DD|FF]{2})`)
	ff := ret.FindAllStringSubmatch(str, -1)
	fmt.Println("ff", ff)

	// //广州 - 天河公园
	// pointA := Coordinates{23.127613278533207, 113.36619463562009}
	// //广州 - 骏唐购物广场
	// pointB := Coordinates{23.125876748465735, 113.38340368865964}

	// fmt.Println("广州-天河公园 A : ", pointA)
	// fmt.Println("广州-骏唐购物广场 B : ", pointB)

	// distance := pointA.Distance(pointB)
	// fmt.Printf("广州-天河公园到广州-骏唐购物广场的距离是 %.2f 公里.\n", distance)

	// data := [][]string{
	// 	[]string{"aaa", "1", "A"},
	// 	[]string{"bbb", "2", "B"},
	// }
	// fmt.Println("data : ", data)

	// emp := []*Employee{}
	// for _, v := range data {
	// 	fmt.Println("data: ", v[0], v[1], v[2])

	// 	// 将 v[1] 转换为整数类型
	// 	age, err := strconv.Atoi(v[1])
	// 	if err != nil {
	// 		fmt.Println(err, v[1])
	// 	}
	// 	e := &Employee{v[0], age, v[2]}
	// 	emp = append(emp, e)
	// }

	// //反射方式
	// for i := 0; i < 2; i++ {
	// 	u := reflect.ValueOf(emp[i])
	// 	name := reflect.Indirect(u).FieldByName("Name")
	// 	age := reflect.Indirect(u).FieldByName("Age")
	// 	job := reflect.Indirect(u).FieldByName("Job")

	// 	fmt.Println(name, age, job)
	// 	fmt.Println("==========")
	// }

	// //反转切片/数组顺序
	// arr := [...]string{"aaa", "bbb", "ccc", "ddd", "eee", "fff"}
	// fmt.Println("原始数据 : ", arr)

	// reversed := []string{}
	// //相反的顺序,并附加到新切片中
	// for i := range arr {
	// 	n := arr[len(arr)-1-i]

	// 	//完整性检查
	// 	//fmt.Println(n)
	// 	reversed = append(reversed, n)
	// }
	// fmt.Println("反转数据 : ", reversed)

	// mapList := make([]map[int]int, 4)
	// mapList[0] = map[int]int{1: 1}
	// mapList[1] = map[int]int{2: 2}
	// mapList[2] = map[int]int{3: 3}
	// mapList[3] = map[int]int{4: 4}
	// //随机洗牌
	// shuffled := models.ShuffleList(0, len(mapList))

	// fmt.Printf("map数据  :  %v\n", mapList)
	// fmt.Printf("洗牌后的map数据  :  %v\n", shuffled)

	// str := "Don't you see the water of the Yellow River coming from the sky, rushing to the sea and not returning"
	// fmt.Printf("不换行数据 : [%v] \n", str)
	// //限制在3个字以内
	// fmt.Println("换行数据 : ", models.WordWrap(str, 3))

	// //日期时间戳比较
	// //对比值 当前时间
	// now := time.Now()
	// fmt.Println("当前时间: ", now)

	// fmt.Println("24小时制 日期: ", now.Format("2006-01-02 15:04:05"))
	// fmt.Println("英式日期: ", now.Format("Mon, Jan 2, 2006 at 3:04pm"))

	// //对比值 固定日期：2010-05-18 23:00:00 +0000 UTC
	// timeAgo := time.Date(2010, time.May, 18, 23, 0, 0, 0, time.UTC)
	// fmt.Println("timeAgo: ", timeAgo)

	// //将时间与 time.Equal() 进行比较
	// sameTime := timeAgo.Equal(now)
	// fmt.Println("timeAgo跟now两个时间对比 是否等值: ", sameTime)

	// //两值 计算今天的时间差
	// diff := now.Sub(timeAgo)
	// fmt.Println("timeAgo跟now两个时间差 :", diff)
	// //or 两函数一样的效果 now.Sub === time.Since
	// diff1 := time.Since(timeAgo)
	// fmt.Println("timeAgo跟now两个时间差 :", diff1)

	// //将 diff 转换为天数
	// days := int(diff.Hours() / 24)

	// fmt.Println("timeAgo:2010-05-18到现在有多少天？", days)

	// //并集
	// a := []string{"Hello", "No", "Zongscan"}
	// b := []string{"Hello", "Yes", "Zongscan"}

	// d := []string{}
	// d = append(a, b...)
	// fmt.Println("合集1: ", d)

	// //切片去重 ： 把上面的两切片合集数据重复的只留一个
	// //可以写成公共函数
	// m := make(map[string]struct{}, 0)
	// newD := make([]string, 0)
	// for _, v1 := range d {
	// 	if _, ok := m[v1]; !ok {
	// 		newD = append(newD, v1)
	// 		m[v1] = struct{}{}
	// 	}
	// }
	// fmt.Println("合集处理后的并集: ", newD)

	// //两个切片或数组中查找共性交集示例
	// // 切片交集ces1
	// a := []string{"Hello", "No", "Zongscan"}
	// b := []string{"Hello", "Yes", "Zongscan"}

	// d := []string{}
	// for i := range a {
	// 	if a[i] == b[i] {
	// 		d = append(d, a[i])
	// 	}
	// }
	// fmt.Println("共性交集1: ", d)

	// // 切片交集ces2
	// a_one := []string{"Hello", "No", "Zongscan"}
	// b_two := []string{"Hello", "Yes", "Zongscan"}
	// d2 := []string{}
	// for _, v1 := range a_one {
	// 	isAdd := false
	// 	for _, v2 := range b_two {
	// 		if v1 == v2 {
	// 			isAdd = true
	// 		}
	// 	}
	// 	if isAdd {
	// 		d2 = append(d2, v1)
	// 	}
	// }
	// fmt.Println("共性交集2: ", d2)

	//查找最大值
	// arr := []int{2, 99, 33, 7, 40, 88}
	// // 假设第一个值是最小的
	// max := arr[0]
	// for _, value := range arr {
	// 	if value > max {
	// 		//找到另一个较小的值，两值比较 替换小值
	// 		max = value
	// 	}
	// }
	// fmt.Println("最大值是 : ", max)

	// file := "logs/gblog.log"
	// info, _ := os.Stat(file)
	// qx := info.Mode()
	// //获取logs/gblog.log文件的权限
	// fmt.Println(file, "文件的权限是", qx)

	//方法：
	//使用os.OpenFile()函数并使用os.IsPermission()检查错误以查看文件是否有权限被拒绝错误。
	//检查文件是否具有（读权限：os.O_RDONLY | 写权限：os.O_WRONLY）
	// f, err := os.OpenFile(file, os.O_RDONLY, 0666)
	// if err != nil {
	// 	if os.IsPermission(err) {
	// 		fmt.Println("没有（读取|写入）权限", f)
	// 		fmt.Println(err)
	// 		os.Exit(1)
	// 	}
	// }
	// f.Close()

	//更改文件的读取、写入或执行权限
	// err := os.Chmod(file, 0777)
	// if err != nil {
	// 	fmt.Println(err)
	// }

}
