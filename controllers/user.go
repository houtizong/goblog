package controllers

import (
	"fmt"
	"gblog/models"
	"path"
	"time"

	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/utils/captcha"

	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
	_ "github.com/go-sql-driver/mysql"
)

// UserController  is a test control
type UserController struct {
	//beego.Controller
	BaseController
}

//验证码
var cpt *captcha.Captcha

func init() {
	store := cache.NewMemoryCache()
	cpt = captcha.NewWithFilter("/captcha/", store)
}

//个人中心
func (c *UserController) User() {
	//获取
	username := c.GetSession("username")

	//3.查询账号密码是否正确
	o := orm.NewOrm()
	user := models.Users{}
	user.Name = username.(string)
	err := o.Read(&user, "Name")
	if err != nil {
		c.TplName = "login.html"
		return
	}

	//c.SetSession("aaa", user)
	//aaa := (c.GetSession("aaa")).(models.Users)
	//fmt.Println(aaa, aaa.Name)

	c.Ctx.WriteString("登陆成功，老铁欢迎您 - 我这里就不做跳转了，自行去本站首页")
	c.Data["json"] = user
	c.ServeJSON()
	c.StopRun()
}

//编辑用户信息  头像
func (c *UserController) Edituserface() {

	o := orm.NewOrm()
	user := models.Users{}

	//获取主键
	username := c.GetSession("username")
	o.Raw("SELECT * FROM users WHERE name = ?", username).QueryRow(&user)
	c.Data["user"] = user

	//判断是否post
	if c.Ctx.Request.Method == "POST" {
		fmt.Println("post1")
		//获取上传文件
		f, fh, err := c.GetFile("face")

		if err != nil {
			//fmt.Println("get file error ", err)
			c.ErrorJson(500, err.Error(), nil)
		}
		defer f.Close()

		//生成文件夹
		timeObj := time.Now()
		//格式化时间为字符串,只保留年月日
		var folder = timeObj.Format("20060102")

		// 保存位置在static/img/face/, 没有文件夹要先创建 需用到os
		apath := "static/img/"
		bpath := "face/"

		//获取文件后缀
		var suf string
		suf = path.Ext(fh.Filename)
		//生成文件名 （即相对地址）
		fname := bpath + folder + models.RandomIntString(4) + suf

		facepath := apath + fname
		//上传
		c.SaveToFile("face", facepath)

		//入库
		user.Profile_photo_path = fname
		models.EditUser(&user)

		fmt.Println(fh.Filename, facepath, fname)

		c.SuccessJson("上传成功")
	}

	c.TplName = "edituserface.html"
}

//编辑用户信息  密码...
func (c *UserController) Edituserpwd() {
	//获取get值
	password := c.GetString("password")

	o := orm.NewOrm()
	user := models.Users{}

	//获取主键
	username := c.GetSession("username")
	o.Raw("SELECT * FROM users WHERE name = ?", username).QueryRow(&user)
	c.Data["user"] = user

	if c.Ctx.Request.Method == "POST" {
		if password == "" {
			c.ErrorJson(500, "密码不能为空", nil)
		}

		//入库
		user.Password = password
		models.EditUser(&user)
		c.SuccessJson("修改成功")
	}

	c.TplName = "edituserpwd.html"
}

//注册页模板
func (c *UserController) Reg() {
	c.TplName = "register.html" //只有加了这句，前端才会跳转到注册页面

	//get All url:http://127.0.0.1:8080/reg?aaa=1&bbb=2
	// {
	//   "aaa": [
	//     "1"
	//   ],
	//   "bbb": [
	//     "2"
	//   ]
	// }
	// data := c.Ctx.Input.Context.Request.URL.Query()
	// c.Data["json"] = data
	// c.ServeJSON()
	// c.StopRun()
}

//注册用户  模型添加数据
func (c *UserController) Register() {
	//1.拿到数据
	name := c.GetString("name")
	email := c.GetString("email")
	password := c.GetString("password")

	//post All
	// data := c.Ctx.Input.Context.Request.Form
	// c.Data["json"] = data
	// c.ServeJSON()
	// c.StopRun()

	//2.对数据进行校验
	valid := validation.Validation{}

	//用户名不空
	isName := valid.Required(name, "name")
	if !isName.Ok {
		c.ErrorJson(500, "用户名不合法", nil)
	}
	//密码 >6<12
	isMinPassword := valid.MinSize(password, 6, "password")
	if !isMinPassword.Ok {
		c.ErrorJson(500, "密码值需大于6位", nil)
	}
	isMaxPassword := valid.MaxSize(password, 12, "password")
	if !isMaxPassword.Ok {
		c.ErrorJson(500, "密码值需小于12位", nil)
	}

	//验证邮箱格式
	isEmail := valid.Email(email, "email")
	if !isEmail.Ok {
		c.ErrorJson(500, "邮箱不合法", nil)
	}
	//TO DO 队列验证邮箱真实性

	//1.验证-验证码
	isCpt := cpt.VerifyReq(c.Ctx.Request)
	if !isCpt {
		c.ErrorJson(500, "验证码不合法", nil)
	}

	//24小时制
	timeObj := time.Now()
	var str = timeObj.Format("2006-01-02 15:04:05")
	user := models.Users{Name: name, Email: email, Password: models.Md5(password), Created_at: str}
	//TO DO
	//里面需在查一遍 做最后层的筛查 比如：数据是否存在|验证邮箱重复 等等

	//插入
	models.AddUser(&user)
	fmt.Println(user)

	c.SuccessJson("注册成功")
	//c.Ctx.WriteString("注册成功!")
}

//登录页模板
func (c *UserController) Login() {
	//判断一下是否登录
	c.TplName = "login.html"
}

//登录post 单条数据查询
func (c *UserController) Info() {
	//获取get值
	name := c.GetString("name")
	password := c.GetString("password")

	//2.判断数据是否合法
	if name == "" || password == "" {
		c.ErrorJson(500, "账号||密码不合法", nil)
	}

	//1.验证-验证码
	isCpt := cpt.VerifyReq(c.Ctx.Request)
	if !isCpt {
		c.ErrorJson(500, "验证码不合法", nil)
	}

	//3.查询账号密码是否正确
	o := orm.NewOrm()
	user := models.Users{}
	user.Name = name
	user.Password = models.Md5(password)
	err := o.Read(&user, "Name", "Password")
	if err != nil {
		c.ErrorJson(500, "账号密码错误", nil)
	} else {
		c.SetSession("username", name)
		c.SetSession("userid", user.Id)
		c.SetSession("useremail", user.Email)
	}

	c.SuccessJson("登录成功")

	//4.跳转 改前端跳
	//c.Ctx.Redirect(302, "/user")
}

//退出
func (c *UserController) Logout() {
	//删除指定session值
	c.DelSession("username")
	c.DelSession("userid")
	c.DelSession("useremail")
	c.Ctx.Redirect(302, "/")
}

//使用原生sql进行列表数据查询
func (c *UserController) InfoList() {

	name := c.GetString("name")
	c.Data["json"] = name
	c.ServeJSON()

	o := orm.NewOrm()
	var maps []orm.Params
	sql := `select * from users`
	o.Raw(sql).Values(&maps)

	//数据集中添加一个随机数 字段
	for _, v := range maps {
		v["rand"] = models.RandomInt(200, 800)
	}

	c.Data["json"] = maps
	c.ServeJSON()
}

/** 发邮件 */
func (c *UserController) SendEmail() {

	f := models.SendEmail("houtizong@dingtalk.com")
	fmt.Printf("send mail %v:", f)
	c.Data["json"] = f
	c.ServeJSON()
}
