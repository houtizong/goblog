package controllers

import (
	"fmt"
	"gblog/models"
	"strconv"

	"strings"

	"github.com/astaxie/beego/orm"
)

type TagsController struct {
	BaseController
}

func (c *TagsController) Tags() {

	c.Data["title"] = "标签,标签列表页-侯体宗的博客"
	c.Data["keywords"] = "标签,标签列表页"
	c.Data["description"] = "标签,个人博客,技术博客,侯体宗的博客,编程,PHP编程,GO编程,数据库,centos,Thinkphp,Hyperf,laravel,beego,mysql,layui,easyui,bootstrap,项目架构分享,侯体宗的博客常用框架及前端后端分享交流"

	o := orm.NewOrm()

	//当前页码
	page, err := c.GetInt("page")
	if err != nil {
		page = 1
		println(err)
	}

	//文章总条数
	var count []orm.Params
	numssql := `select count(*) as count from tag group by tagname order by tag_id desc`
	o.Raw(numssql).Values(&count)
	nums := count[0]["count"]
	// 这里先显示转换，.(string) 把interface转换成string类型，再利用strconv.Atoi把string 转换成int
	cnt, _ := strconv.Atoi(nums.(string))
	//当前页 每页条数 总条数
	var pageSize int
	pageSize = 300
	pages := models.Paginator(page, pageSize, cnt)
	c.Data["pages"] = pages

	//标签列表 分页数据
	var tags []orm.Params
	tagssql := `select * from tag group by tagname order by tag_id desc limit ?,?`
	o.Raw(tagssql, (page-1)*pageSize, pageSize).Values(&tags)
	c.Data["tags"] = tags

	//文章归档 格式：2017年01月
	var artdates []orm.Params
	artdatessql := `select pubtime,FROM_UNIXTIME( pubtime,'%Y-%m') as time ,count(*) as num FROM art where  is_state=0 and is_del=1 group by time`
	o.Raw(artdatessql).Values(&artdates)
	c.Data["artdates"] = artdates

	c.TplName = "tags.html"
}

func (c *TagsController) Tag() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.Atoi(idStr)

	o := orm.NewOrm()

	tag := models.Tag{}
	o.Raw("SELECT * FROM tag WHERE tag_id = ?", id).QueryRow(&tag)
	c.Data["tag"] = tag

	c.Data["title"] = tag.Tagname + "-侯体宗的博客"
	c.Data["keywords"] = tag.Tagname
	c.Data["description"] = tag.Tagname

	//获取最多20个文章id
	var artids []orm.Params
	o.Raw("SELECT art_id FROM tag WHERE tagname = ? limit 20", tag.Tagname).Values(&artids)

	//map转切片
	ids := []string{}
	for _, v := range artids {
		ids = append(ids, v["art_id"].(string))
	}
	//切片转str
	idsstr := strings.Join(ids, "','")
	fmt.Println(idsstr)

	//文章  where in() 写法
	var arts []orm.Params
	artssql := `select art_id,title,pubtime,view FROM art where art_id in ('%s') and is_state=0 and is_del=1 order by pubtime`
	//处理占位符?
	artssql = fmt.Sprintf(artssql, idsstr)
	o.Raw(artssql).Values(&arts)
	c.Data["arts"] = arts

	//c.Data["json"] = arts
	//c.ServeJSON()
	//c.StopRun()

	//文章归档 格式：2017年01月
	var artdates []orm.Params
	artdatessql := `select pubtime,FROM_UNIXTIME( pubtime,'%Y-%m') as time ,count(*) as num FROM art where  is_state=0 and is_del=1 group by time`
	o.Raw(artdatessql).Values(&artdates)
	c.Data["artdates"] = artdates

	c.TplName = "tag.html"
}
