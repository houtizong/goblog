package admin

import (
	"fmt"
	"gblog/models"
	"strconv"
	"time"

	"github.com/astaxie/beego/orm"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/toolbox"
)

type TasksController struct {
	beego.Controller
}

func (c *TasksController) URLMapping() {
	c.Mapping("Tasks", c.Tasks)
	c.Mapping("Task", c.Task)
	c.Mapping("Stoptask", c.Stoptask)
	c.Mapping("Starttask", c.Starttask)
}

// @router /admin/tasks [get]
func (this *TasksController) Tasks() {
	o := orm.NewOrm()
	//当前页码
	page, err := this.GetInt("page")
	if err != nil {
		page = 1
		println(err)
	}

	//文章总条数
	var count []orm.Params
	numssql := `select count(*) as count from task order by id desc`
	o.Raw(numssql).Values(&count)
	nums := count[0]["count"]
	// 这里先显示转换，.(string) 把interface转换成string类型，再利用strconv.Atoi把string 转换成int
	cnt, _ := strconv.Atoi(nums.(string))
	//当前页 每页条数 总条数
	var pageSize int
	pageSize = 5
	pages := models.Paginator(page, pageSize, cnt)
	this.Data["pages"] = pages
	this.Data["total"] = cnt

	//分页数据
	var tasks []orm.Params
	taskssql := `select id,taskname,spec,addtime,status from task order by id desc limit ?,?`
	o.Raw(taskssql, (page-1)*pageSize, pageSize).Values(&tasks)
	this.Data["tasks"] = tasks

	//this.Data["json"] = pages
	//this.ServeJSON()
	//this.StopRun()

	this.TplName = "admin/tasks.html"
}

// @router /admin/task [get]
func (this *TasksController) Task() {
	//TODO 全部启动 - 初始化
}

// @router /admin/starttask [post]
func (this *TasksController) Starttask() {
	idStr := this.GetString("id")
	id, _ := strconv.Atoi(idStr)
	o := orm.NewOrm()
	task := models.Task{Id: id}
	o.Read(&task)

	//初始化 - 启动指定任务
	tk := toolbox.NewTask(task.Taskname, task.Spec,
		func() error {
			fmt.Println(task.Taskname, "任务开始", time.Now().Format("2006-01-02 15:04:05"))
			return nil
		})
	//检测
	err := tk.Run()
	if err != nil {
		fmt.Println(err)
	}
	//加入任务列表
	toolbox.AddTask(task.Taskname, tk)
	//执行任务
	toolbox.StartTask()

	//更新状态
	task.Status = 1
	models.EditTask(&task)

	this.Data["json"] = "已启动"
	this.ServeJSON()
	this.StopRun()
	//time.Sleep(time.Minute * 5)
	//toolbox.StopTask()
}

// @router /admin/stoptask [post]
func (this *TasksController) Stoptask() {
	idStr := this.GetString("id")
	id, _ := strconv.Atoi(idStr)
	o := orm.NewOrm()
	task := models.Task{Id: id}
	err := o.Read(&task)
	if err != nil {
		fmt.Println(task.Taskname, "任务不存在")
		return
	}

	//删除指定任务
	toolbox.DeleteTask(task.Taskname)
	fmt.Println(task.Taskname, "任务已停止")

	//更新状态
	task.Status = 0
	models.EditTask(&task)

	this.Data["json"] = "已停止"
	this.ServeJSON()
	this.StopRun()
}
