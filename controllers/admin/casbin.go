package admin

import (
	"fmt"
	"gblog/middleware"
	"gblog/models"
	"strconv"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

type CasbinController struct {
	beego.Controller
}

func (c *CasbinController) URLMapping() {
	c.Mapping("Users", c.Users)
	c.Mapping("Roles", c.Roles)
	c.Mapping("Rules", c.Rules)
	c.Mapping("Rule_add", c.Rule_add)
}

// @router /admin/casbin/users [get]
func (this *CasbinController) Users() {
	o := orm.NewOrm()
	//当前页码
	page, err := this.GetInt("page")
	if err != nil {
		page = 1
		fmt.Println(err)
	}

	//文章总条数
	nums, _ := o.QueryTable("admin").Count() // SELECT COUNT(*) FROM USER
	// 这里先显示转换，strconv.FormatInt把int64转换成string类型，再利用strconv.Atoi把string 转换成int
	cnt, _ := strconv.Atoi(strconv.FormatInt(nums, 10))
	//当前页 每页条数 总条数
	var pageSize int
	pageSize = 5
	pages := models.Paginator(page, pageSize, cnt)
	this.Data["pages"] = pages
	this.Data["total"] = cnt

	//分页数据
	var admins []models.Admin
	o.QueryTable("admin").Limit(pageSize, (page-1)*pageSize).All(&admins)
	this.Data["admins"] = admins

	//角色查询
	var roles []models.Role
	o.QueryTable("role").All(&roles)
	this.Data["roles"] = roles

	this.TplName = "admin/casbin/user_index.html"
}

// @router /admin/casbin/roles [get]
func (this *CasbinController) Roles() {
	o := orm.NewOrm()
	//角色查询
	var roles []models.Role
	o.QueryTable("role").All(&roles)
	this.Data["roles"] = roles

	//权限规则
	var casbinrules []models.CasbinRule
	o.QueryTable("casbin_rule").All(&casbinrules)
	this.Data["casbinrules"] = casbinrules

	this.TplName = "admin/casbin/role_index.html"
}

// @router /admin/casbin/rules [get]
func (this *CasbinController) Rules() {
	o := orm.NewOrm()
	//角色查询
	var roles []models.Role
	o.QueryTable("role").All(&roles)
	this.Data["roles"] = roles

	//权限规则
	var casbinrules []models.CasbinRule
	o.QueryTable("casbin_rule").All(&casbinrules)
	this.Data["casbinrules"] = casbinrules

	this.TplName = "admin/casbin/rule_index.html"
}

// @router /admin/casbin/rule_add [post]
func (this *CasbinController) Rule_add() {

	ptype := this.GetString("ptype") //策略
	v0 := this.GetString("v0")       //角色
	v1 := this.GetString("v1")       //资源路由
	v2 := this.GetString("v2")       //操作

	//获取规则变量 进行权限变更
	e := middleware.Enforcer

	if ptype == "p" {
		//AddPolicy 向当前策略添加授权规则
		e.AddPolicy(v0, v1, v2)
	} else {
		//AddGroupingPolicy 向当前策略添加角色继承规则
		e.AddGroupingPolicy(v0, v1)
	}

	this.Data["json"] = map[string]string{"Code": "200", "Msg": "添加成功"}
	this.ServeJSON()
	this.StopRun()
}
