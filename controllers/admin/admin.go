package admin

import (
	"fmt"
	"gblog/models"
	"sync"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

type AdminController struct {
	beego.Controller
}

func (c *AdminController) URLMapping() {
	c.Mapping("Admin", c.Admin)
	c.Mapping("Test", c.Test)
	c.Mapping("Login", c.Login)
	c.Mapping("Info", c.Info)
}

// @router /admin/home [get]
func (this *AdminController) Admin() {
	//单例测试代码 仅执行一次
	//ins := GetIns()
	//ins.Code = "code1"
	//ins1 := GetIns()
	//ins1.Code = "code2"
	//fmt.Println(ins, ins1, this.GetSession("adminid"))

	this.TplName = "admin/index.html"
}

// @router /admin/test [get]
func (this *AdminController) Test() {
	//c.Data["Website"] = "beego.me"
	//c.Data["Email"] = "astaxie@gmail.com"
	//c.TplName = "index.tpl"

	fmt.Println("test")
	this.Ctx.WriteString(`我是controllers下级控制器目录api中的xx方法`)
}

// @router /admin/login [get]
func (this *AdminController) Login() {
	this.TplName = "admin/login.html"
}

// @router /admin/login [post]
func (this *AdminController) Info() {
	//获取get值
	username := this.GetString("username")
	password := this.GetString("password")

	//判断数据是否合法 (正常引入表单验证模块)
	if username == "" || password == "" {
		this.Data["json"] = map[string]string{"Code": "500", "Msg": "账号||密码不合法"}
		this.ServeJSON()
		this.StopRun()
	}

	//验证码

	//3.查询账号密码是否正确
	o := orm.NewOrm()
	user := models.Admin{}
	user.Username = username
	user.Password = models.Md5(password)
	err := o.Read(&user, "Username", "Password")
	if err != nil {
		this.Data["json"] = map[string]string{"Code": "500", "Msg": "账号密码错误"}
		this.ServeJSON()
		this.StopRun()
	} else {
		//把用户数据存入session
		this.SetSession("admins", user)
	}
	//等等...验证

	//最后成功登陆
	this.Data["json"] = map[string]string{"Code": "200", "Msg": "登录成功"}
	this.ServeJSON()
	this.StopRun()
}

//单例测试代码
type Sing struct {
	Code string
	//Children []sss
}

//type Sing map[string]string
//使用make初始化Sing

var (
	ins      *Sing
	loadOnce sync.Once
)

//加入sync.Once 避免了每次加锁，提高代码效率
//单例
func GetIns() *Sing {
	loadOnce.Do(func() {
		ins = &Sing{}
	})
	return ins
}
